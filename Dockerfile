FROM python:3.7

WORKDIR /artline

COPY requirements.txt ./requirements.txt

COPY app.py ./app.py

COPY models/* ./models/

COPY static/css/* ./static/css/
COPY static/fonts/* ./static/fonts/
COPY static/images/* ./static/images/

COPY templates/* ./templates/

COPY result/* ./result/

COPY tmp/* ./tmp/

COPY core/* ./core/

RUN pip3 install -r requirements.txt 

EXPOSE 8080

#ENTRYPOINT ["python3","app.py"]
CMD ["python3", "app.py"]

