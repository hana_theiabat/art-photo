# Turn your photo into Art

## Presentation 

[Link](https://docs.google.com/presentation/d/1jas4875PFcjZVx3IsJY3d7sGIEePwp3MwqtfS5jEDsk/edit?usp=sharing)
 
## How to run?

1. `pip3 install -r requirements.txt`
2. `python3 app.py`

## How to use?

1. Run the Flask app
2. Open `localhost:5000` in browser
3. Select an image( .png or .jpg ) then click `Go Art`
4. Once the process done successfully, you should see the result in browser.

Enjoy and give a star if you like it.

## Docker

* Cd into the project directory
* Build the docker file to create the image
`docker build -t art_image .`
* Create and run the container
`docker run -d --name art_container  -p 8080:8080  art_image`

* Now visit the new url for the API from the running container
* * You can find it by listing the running containers with their information, such as port:
`docker container ls -a `

| CONTAINER ID         |      IMAGE        |        PORTS          |     NAMES     |
| -------------------- |:-----------------:| ---------------------:|--------------:|
| 0b734b565e8f            art_image          0.0.0.0:8080->8080/tcp   art_container|


* visit the link http://0.0.0.0:8080/
