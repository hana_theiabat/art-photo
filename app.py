import base64
import urllib
from uuid import uuid1

import PIL.Image
import torchvision.transforms as T
import matplotlib

# use Agg as backend to not show image in server：
# https://stackoverflow.com/questions/9622163/save-plot-to-image-file-instead-of-displaying-it-using-matplotlib
matplotlib.use('Agg')
from fastai.vision import *
from fastai.vision import load_learner
from flask import Flask, request, render_template
from core import FeatureLoss
import fastai
from fastai.vision import *
from fastai.utils.mem import *
from fastai.vision import open_image, load_learner, image, torch
import numpy as np
import urllib.request
import PIL.Image
from io import BytesIO
import torchvision.transforms as T
from PIL import Image
import requests
from io import BytesIO
import fastai
from fastai.vision import *
from fastai.utils.mem import *
from fastai.vision import open_image, load_learner, image, torch
import numpy as np
import urllib.request
import PIL.Image
from PIL import Image
from io import BytesIO
import torchvision.transforms as T
learn = None
learn1 = None

class FeatureLoss(nn.Module):
    def __init__(self, m_feat, layer_ids, layer_wgts):
        super().__init__()
        self.m_feat = m_feat
        self.loss_features = [self.m_feat[i] for i in layer_ids]
        self.hooks = hook_outputs(self.loss_features, detach=False)
        self.wgts = layer_wgts
        self.metric_names = ['pixel',] + [f'feat_{i}' for i in range(len(layer_ids))
              ] + [f'gram_{i}' for i in range(len(layer_ids))]
 
    def make_features(self, x, clone=False):
        self.m_feat(x)
        return [(o.clone() if clone else o) for o in self.hooks.stored]
    
    def forward(self, input, target):
        out_feat = self.make_features(target, clone=True)
        in_feat = self.make_features(input)
        self.feat_losses = [base_loss(input,target)]
        self.feat_losses += [base_loss(f_in, f_out)*w
                             for f_in, f_out, w in zip(in_feat, out_feat, self.wgts)]
        self.feat_losses += [base_loss(gram_matrix(f_in), gram_matrix(f_out))*w**2 * 5e3
                             for f_in, f_out, w in zip(in_feat, out_feat, self.wgts)]
        self.metrics = dict(zip(self.metric_names, self.feat_losses))
        return sum(self.feat_losses)
    
    def __del__(self): self.hooks.remove()
 
def add_margin(pil_img, top, right, bottom, left, color):
    width, height = pil_img.size
    new_width = width + right + left
    new_height = height + top + bottom
    result = Image.new(pil_img.mode, (new_width, new_height), color)
    result.paste(pil_img, (left, top))
    return result
# singleton start
def load_pkl(self) -> Any:
    global learn
    global learn1
    path = Path(".")
    learn = load_learner(path, 'models/Toonme_new_820.pkl')
    learn1=load_learner(path, 'models/ArtLine_920.pkl')


PklLoader = type('PklLoader', (), {"load_pkl": load_pkl})
pl = PklLoader()
pl.load_pkl()


# singleton end

def demo_show(the_img: Image, ax: plt.Axes = None, figsize: tuple = (3, 3), title: Optional[str] = None,
              hide_axis: bool = True,
              cmap: str = None, y: Any = None, out_file: str = None, **kwargs):
    "Show image on `ax` with `title`, using `cmap` if single-channel, overlaid with optional `y`"
    cmap = ifnone(cmap, defaults.cmap)
    ax = show_image(the_img, ax=ax, hide_axis=hide_axis, cmap=cmap, figsize=figsize)
    if y is not None: y.show(ax=ax, **kwargs)
    if title is not None: ax.set_title(title)
    ax.get_figure().savefig('result/' + out_file)
    plt.close(ax.get_figure())
    print('close')




app = Flask(__name__)


@app.route('/index', methods=['GET'])
@app.route('/', methods=['GET'])
def index_view():
    return render_template('index.html')


def read_img_file_as_base64(local_file) -> str:
    with open(local_file, "rb") as rf:
        base64_str = base64.b64encode(rf.read())
        os.remove(local_file)
        return base64_str.decode()


@app.route('/result', methods=["POST"])
def result_view():
    f = request.files['uimg']
    if f is None:
        return render_template('result.html', error=True)

    local_filename = '{0}{1}'.format(uuid1().hex, f.filename[f.filename.index('.'):len(f.filename)])
    local_file = 'tmp/{0}{1}'.format(uuid1().hex, f.filename[f.filename.index('.'):len(f.filename)])
    f.save(local_file)
    
    
    try:

        #######
        print('local_file: ',local_file)
        img = PIL.Image.open(local_file).convert("RGB")
        im_new = add_margin(img, 150, 150, 150, 150, (255, 255, 255))
        im_new.save("test.jpg", quality=95)

        img = open_image("test.jpg")

        p,img_hr,b = learn1.predict(img)
        x = np.minimum(np.maximum(image2np(img_hr.data*255), 0), 255).astype(np.uint8)
        PIL.Image.fromarray(x).save("tes.jpg",quality=95)
        img_fast = open_image("tes.jpg")

        p, img_hr, b = learn.predict(img_fast)

        x = np.minimum(np.maximum(image2np(img_hr.data*255), 0), 255).astype(np.uint8)
        PIL.Image.fromarray(x).save("x.jpg",quality=95)
        r = PIL.Image.open("x.jpg").convert("RGB")

        result_img_base64 = read_img_file_as_base64("x.jpg")#'result/' + local_filename)
        img_base64 = read_img_file_as_base64("test.jpg")
        result_img2_base64 = read_img_file_as_base64("tes.jpg") 
    

    except Exception  as e:
        return render_template('result.html', error=True)
    finally:
        if os.path.exists(local_file):
            os.remove(local_file)
    

    return render_template('result.html', error=False, result_img=result_img_base64,result_img2=result_img2_base64,img=img_base64)


if __name__ == '__main__':
    app.run(debug=False, host='0.0.0.0',port=8080)
